Lindsay Giguiere is an experienced entrepreneur, influencer and #womensupporting women advocate. Her nearly twenty years of experience spans Personal Care, Health, Wellness, Fashion, the Enjoyment of Shoes and Life-coaching.

Website : https://www.lindsaygiguiere.com/